using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchingObjects : MonoBehaviour
{
  
    public TypeOFjar matchintTypeOfJar;
    public bool matchingJarInPlace = false;

    public MatchingObjectController myMatchingObjectController;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        MathcingJar jar = collision.GetComponent<MathcingJar>();

        if (jar != null && matchintTypeOfJar == jar.myTypeOfJar)
        {
            matchingJarInPlace = true;
            myMatchingObjectController.CheckAllObjectsActivated();

        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        MathcingJar jar = collision.GetComponent<MathcingJar>();

        if (jar != null && matchintTypeOfJar == jar.myTypeOfJar)
        {
            matchingJarInPlace = false;
        }
    }
}