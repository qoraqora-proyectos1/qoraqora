using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFly : MonoBehaviour
{
    public float speed = 3f; // Velocidad de movimiento del enemigo
    public float detectionRange = 5f; // Rango de detecci�n del jugador

    private Transform player; // Referencia al transform del jugador

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform; // Buscar el objeto con la etiqueta "Player" y obtener su transform
    }

    private void Update()
    {
        if (player != null)
        {
            // Calcular la distancia entre el enemigo y el jugador
            float distance = Vector3.Distance(transform.position, player.position);

            // Si el jugador est� dentro del rango de detecci�n
            if (distance <= detectionRange)
            {
                // Calcular la direcci�n hacia el jugador
                Vector3 direction = player.position - transform.position;
                direction.Normalize();

                // Mover al enemigo en la direcci�n del jugador
                transform.Translate(direction * speed * Time.deltaTime);
            }
        }
    }
}
