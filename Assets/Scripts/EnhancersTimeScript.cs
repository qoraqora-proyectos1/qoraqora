using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnhancersTimeScript : MonoBehaviour
{
    public float timeIncreaseAmount = 100f; // Cantidad de aumento de tiempo al recoger el objeto
    public TimeBar timeBar;

    public GameObject health;
    public GameObject strenght;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (timeBar != null)
            {
                timeBar.IncreaseTime(timeIncreaseAmount);
                Debug.Log("Aumenta el tiempo por el potenciador");

                Destroy(health);
                Destroy(strenght);
                Destroy(gameObject); // Destruir el objeto al ser recogido
            }
        }
    }
}
