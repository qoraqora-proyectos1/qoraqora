using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterStatue : MonoBehaviour
{
    public Vector2 detectionRange;
    public Transform target;
    public GameObject statueProjectile;
    public Transform shootPoint;
    public float projectileForce = 100f;
    public AudioSource statueShootSound;

    private bool detected = false;
    private float nextShootTime;

    private void Start()
    {
        nextShootTime = Time.time + 1.5f;
    }

    private void Update()
    {
        if (Time.time >= nextShootTime)
        {
            DetectPlayer();
            Shoot();
            nextShootTime = Time.time + 1.5f;
        }
    }

    private void DetectPlayer()
    {
        Vector2 targetPos = target.position;
        Vector2 shootPointPos = shootPoint.position;

        // Calcular las coordenadas m�nimas y m�ximas del rango rectangular
        float minX = shootPointPos.x - detectionRange.x / 2f;
        float maxX = shootPointPos.x + detectionRange.x / 2f;
        float minY = shootPointPos.y - detectionRange.y / 2f;
        float maxY = shootPointPos.y + detectionRange.y / 2f;

        // Verificar si el jugador se encuentra dentro del rango rectangular
        if (targetPos.x >= minX && targetPos.x <= maxX && targetPos.y >= minY && targetPos.y <= maxY)
        {
            detected = true;
        }
        else
        {
            detected = false;
        }
    }

    private void Shoot()
    {
        if (detected)
        {
            GameObject projectile = Instantiate(statueProjectile, shootPoint.position, Quaternion.identity);
            Rigidbody2D projectileRigidbody = projectile.GetComponent<Rigidbody2D>();
            projectileRigidbody.AddForce(Vector2.right * projectileForce);
            statueShootSound.Play();
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireCube(transform.position, detectionRange);
    }
}