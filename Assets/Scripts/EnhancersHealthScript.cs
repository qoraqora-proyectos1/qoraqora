using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnhancersHealthScript : MonoBehaviour
{
    public float healthIncreaseAmount = 20f; // Cantidad de aumento de salud al recoger el objeto
    public GameObject time;
    public GameObject strenght; 

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            if (playerHealth != null)
            {
                playerHealth.IncreaseHealth(healthIncreaseAmount);
                Debug.Log("Aumenta la vida por el potenciador");

                Destroy(time);
                Destroy(strenght);
                Destroy(gameObject); // Destruir el objeto al ser recogido
            }
        }
    }
}
