using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : MonoBehaviour
{
    public int maxHealth = 100; // Vida m�xima del enemigo
    public int currentHealth; // Vida actual del enemigo
    public float damageAmount = 10f; // Cantidad de da�o que el enemigo recibe al ser impactado por el bullet
    public bool isAlive = true;

    public GameObject victoryScreen;

    public GameObject bars;
    public GameObject inventory;

    public AudioSource victoryAudio;

    public GameObject victory;

    public GameObject menubutton;


    private void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        isAlive = false;
        Destroy(gameObject);

        victoryScreen.SetActive(true);
        bars.SetActive(false);
        menubutton.SetActive(false);
        inventory.SetActive(false);
        victoryAudio.Play();
        victory.SetActive(true);

    }
}