using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausaMenuManager : MonoBehaviour
{
    public void LoadMenuScene()
    {
        SceneLoaderSimple.Instance.LoadMainMenu();
        SceneLoaderSimple.Instance.RemoveGameScene();
    }
}
