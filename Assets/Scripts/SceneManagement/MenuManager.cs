using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public void LoadGameScene()
    {
        Time.timeScale = 1.0f;
        SceneLoaderSimple.Instance.LoadGameScene();
        SceneLoaderSimple.Instance.RemoveMainMenu();
    }
}
