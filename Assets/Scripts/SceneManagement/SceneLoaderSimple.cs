using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderSimple : MonoBehaviour
{
    [SerializeField] private string staticScene = "StaticScene";
    [SerializeField] private string menuScene = "MenuScene";
    [SerializeField] private string gameScene = "GameScene";

    private static SceneLoaderSimple instance;
    public static SceneLoaderSimple Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
        LoadMainMenu();
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadSceneAsync(menuScene, LoadSceneMode.Additive);
    }

    public void RemoveMainMenu()
    {
        SceneManager.UnloadSceneAsync(menuScene);
    }

    public void LoadGameScene()
    {
        SceneManager.LoadSceneAsync(gameScene, LoadSceneMode.Additive);

    }
    public void RemoveGameScene()
    {
        SceneManager.UnloadSceneAsync(gameScene);
    }
}

