using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Door3 : MonoBehaviour
{
    public Animator doorAnimator;
    public AudioSource openDoor;
    public List<TorchIndividual> torches; // Lista de referencias a las antorchas

    private bool isOpen = false;

    private void Update()
    {
        bool allTorchesActivated = true;

        foreach (TorchIndividual torch in torches)
        {
            if (!torch.activated)
            {
                allTorchesActivated = false;
                break;
            }
        }

        if (allTorchesActivated && !isOpen)
        {
            OpenDoor();
            openDoor.Play();
        }
    }


    private void OpenDoor()
    {
        doorAnimator.SetBool("Open", true);
        StartCoroutine(DestroyDoorAfterDelay(2f));
    }

    private IEnumerator DestroyDoorAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}