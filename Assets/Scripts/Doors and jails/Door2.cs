using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door2: MonoBehaviour
{
    public Animator doorAnimator;
    public ObjectScript objectScript;
    public AudioSource openDoor;

    private bool isOpen = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && objectScript.keyAvailable && !isOpen)
        {
            OpenDoor();
            openDoor.Play();
        }
    }

    private void OpenDoor()
    {
        doorAnimator.SetBool("Open", true);
        isOpen = true;
        objectScript.keyAvailable = false;

        objectScript.inventoryKey.SetActive(false);

        StartCoroutine(DestroyDoorAfterDelay(2f));
    }

    private IEnumerator DestroyDoorAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}
