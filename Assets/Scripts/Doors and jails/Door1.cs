using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door1 : MonoBehaviour
{
    public Animator doorAnimator;
    public AudioSource openDoor;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            OpenDoor();
            openDoor.Play();
        }
    }

    private void OpenDoor()
    {
        doorAnimator.SetBool("Open", true);
        StartCoroutine(DestroyDoorAfterDelay(2f));
    }

    private IEnumerator DestroyDoorAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}


