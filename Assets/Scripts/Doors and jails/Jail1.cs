using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jail1 : MonoBehaviour
{
    public Animator jailAnimator;
    public ObjectScript objectScript;
    public EnemyHealth worm;
    public EnemyHealth fly;
    public AudioSource openJail;

    private void Update()
    {
        if (worm.isAlive == false && fly.isAlive == false)
        {
            OpenDoor();
            openJail.Play();
            
        }
    }
   
    private void OpenDoor()
    {
        jailAnimator.SetBool("Open", true);
        StartCoroutine(DestroyDoorAfterDelay(2f));
    }

    private IEnumerator DestroyDoorAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}

