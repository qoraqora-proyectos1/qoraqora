using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchSpikes : MonoBehaviour
{
    public class TorchIndividual : MonoBehaviour
    {

        public bool activated; // Variable para indicar si la antorcha ha sido activada

        public Animator torchAnimator;

        public AudioSource fire;

        public GameObject spikes;

        public void ActivateTorch()
        {
            activated = true;
            torchAnimator.SetBool("On", true);
            fire.Play();

        }

        private void Update()
        {
            if (activated == true)
            {
                spikes.SetActive(false);
            }
        }
    }
}