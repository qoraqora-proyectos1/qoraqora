using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogue : MonoBehaviour
{
    private bool isPlayerInRange;
    private bool didDialogueStart;
    private float typingTime = 0.05f;
    private int lineIndex;

    public GameObject panelInventory;
    public GameObject bars;

    //public GameObject simbolo;
    public GameObject dialoguePanel;
    [SerializeField] private TMP_Text dialogueText;
    [SerializeField, TextArea(4,6)] private string[] dialogueLines;

    public PlayerController playerController;

    public GameObject textTikan;

    void Update()
    {
        if (isPlayerInRange)
        {
            textTikan.SetActive(true);

        }

        if (!isPlayerInRange)
        {
            textTikan.SetActive(false);
        }

        if (isPlayerInRange && Input.GetKeyDown("k"))
        {


            if (!didDialogueStart)
            {
                StartDialogue();
            }
            else if (dialogueText.text == dialogueLines[lineIndex])
            {
                
                NextDialogueLine();
            }
            else
            {
                StopAllCoroutines();
  
                dialogueText.text = dialogueLines[lineIndex];

            }
        }
    }

    private void StartDialogue()
    {     
        didDialogueStart = true;
        dialoguePanel.SetActive(true);
        //simbolo.SetActive(false);
        playerController.canMove = false;
        playerController.canShoot = false;

        panelInventory.SetActive(false);
        bars.SetActive(false);

        lineIndex = 0;
        StartCoroutine(ShowLine());

    }

    private void NextDialogueLine()
    {
        lineIndex++;
        if(lineIndex < dialogueLines.Length)
        {
            StartCoroutine(ShowLine());
        }
        else
        {
            FindObjectOfType<PlayerController>().isActive = true;
            didDialogueStart = false;
            dialoguePanel.SetActive(false);
            panelInventory.SetActive(true);
            bars.SetActive(true);
            playerController.canMove = true;
            playerController.canShoot = true;
            //simbolo.SetActive(true);
        }
    }

    private IEnumerator ShowLine()
    {
       // FindObjectOfType<MovimientoProtagonista>().isActive = false;
        dialogueText.text = string.Empty;

        foreach(char ch in dialogueLines[lineIndex])
        {
            dialogueText.text += ch;
            yield return new WaitForSeconds(typingTime);
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = true;
            //simbolo.SetActive(true);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = false;
            //simbolo.SetActive(false);
        }

    }


}
