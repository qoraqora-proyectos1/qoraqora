using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObjectText: MonoBehaviour
{
    private bool isPlayerInRange;
    private bool didDialogueStart;
    private float typingTime = 0.05f;
    private int lineIndex;
    //public GameObject simbolo;
    public GameObject dialoguePanel;
    [SerializeField] private TMP_Text dialogueText;
    [SerializeField, TextArea(4,6)] private string[] dialogueLines;

    public PlayerController playerController;


    void Update()
    {
        if (isPlayerInRange)
        {
            playerController.canShoot = false;

        }

        if (!isPlayerInRange)
        {
            playerController.canShoot = true;

        }

        if (isPlayerInRange)
        {
            playerController.canMove = false;

            if (!didDialogueStart)
            {
                StartDialogue();
            }
            else if (dialogueText.text == dialogueLines[lineIndex])
            {
                
                NextDialogueLine();
            }
            else
            {
                StopAllCoroutines();
  
                dialogueText.text = dialogueLines[lineIndex];

            }
        }
    }

    private void StartDialogue()
    {     
        didDialogueStart = true;
        dialoguePanel.SetActive(true);
        //simbolo.SetActive(false);
        lineIndex = 0;
        StartCoroutine(ShowLine());
    }

    private void NextDialogueLine()
    {
        lineIndex++;
        if(lineIndex < dialogueLines.Length)
        {
            StartCoroutine(ShowLine());
        }
        else
        {
            FindObjectOfType<PlayerController>().isActive = true;
            didDialogueStart = false;
            dialoguePanel.SetActive(false);
            //simbolo.SetActive(true);
        }
    }

    private IEnumerator ShowLine()
    {
        dialogueText.text = string.Empty;

        foreach(char ch in dialogueLines[lineIndex])
        {
            dialogueText.text += ch;
            yield return new WaitForSeconds(typingTime);
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = true;
            //simbolo.SetActive(true);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = false;
            //simbolo.SetActive(false);
        }

    }


}
