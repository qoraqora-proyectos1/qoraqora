using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchIndividual : MonoBehaviour
{
    public int index; // �ndice de la antorcha

    public bool activated; // Variable para indicar si la antorcha ha sido activada

    public Animator torchAnimator;

    public AudioSource fire;

    public void ActivateTorch()
    {
        activated = true;
        Debug.Log("Antorcha " + index + " activada");
        torchAnimator.SetBool("On", true);
        fire.Play();

    }
}