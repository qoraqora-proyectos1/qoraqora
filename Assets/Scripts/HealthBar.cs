using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Image healthBarImage; // Referencia al componente Image de la barra de vida
    public PlayerHealth playerHealth;

    public void UpdateBar()
    {
        // Calcular el progreso actual de la barra de vida
        float progress = playerHealth.currentHealth / playerHealth.maxHealth;

        // Actualizar el relleno de la imagen de la barra de vida en funci�n del progreso
        healthBarImage.fillAmount = progress;
    }
}

