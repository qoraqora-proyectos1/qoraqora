using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage: MonoBehaviour
{
    public float damageAmount = 10f; // Cantidad de da�o que inflige el enemigo
    public PlayerHealth playerHealth; // Referencia al script PlayerHealth
    public HealthBar healthBar; // Referencia al script PlayerHealth

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();

            if (playerHealth != null)
            {
                playerHealth.TakeDamage(damageAmount);
                Debug.Log("El personaje pierde vida");
            }
        }
    }
}

