using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JarPosition : MonoBehaviour
{
    public Transform[] objectsToRecord; // Arreglo de objetos cuyas posiciones se van a almacenar
    public List <Vector3> initialPositions; // Arreglo de las posiciones iniciales de los objetos

    public Button resetButton; // Referencia al bot�n de reseteo


    private void Start()
    {
        initialPositions = new List<Vector3>();
        

        for (int i = 0; i < objectsToRecord.Length; i++)
        {
            initialPositions.Add(objectsToRecord[i].position);
            Debug.Log("berag");

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Verificar si el jugador ha pasado por el collider
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Dentro del collider");

            // Almacenar las posiciones iniciales de los objetos
            StoreInitialPositions();

            // Mostrar el bot�n de reseteo
            resetButton.gameObject.SetActive(true);
            resetButton.interactable = true;
        }
    }

    private void StoreInitialPositions()
    {
        // Almacenar las posiciones iniciales de los objetos
        for (int i = 0; i < objectsToRecord.Length; i++)
        {
            initialPositions[i] = objectsToRecord[i].position;
        }
    }

    public void ResetObjects()
    {
        // Restaurar las posiciones iniciales de los objetos
        for (int i = 0; i < objectsToRecord.Length; i++)
        {
            objectsToRecord[i].position = initialPositions[i];
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        // Verificar si el jugador ha salido del collider
        if (other.CompareTag("Player"))
        {
            // Ocultar el bot�n de reseteo
            resetButton.gameObject.SetActive(false);
            resetButton.interactable = false;
        }
    }
}