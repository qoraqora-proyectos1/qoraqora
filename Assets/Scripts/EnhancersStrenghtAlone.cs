using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnhancersStrenghtAlone : MonoBehaviour
{

    public PlayerBullet playerBullet;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerBullet.damageAmount = 20;
            Debug.Log("Aumenta la fuerza por el potenciador");

            Destroy(gameObject); // Destruir el objeto al ser recogido

        }
    }
}
