using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnhancersStrnghtScript : MonoBehaviour
{

    public PlayerBullet playerBullet;
    public GameObject time;
    public GameObject health;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            playerBullet.damageAmount = 20;
            Debug.Log("Aumenta la fuerza por el potenciador");

            Destroy(time);
            Destroy(health);
            Destroy(gameObject); // Destruir el objeto al ser recogido

        }
    }
}
