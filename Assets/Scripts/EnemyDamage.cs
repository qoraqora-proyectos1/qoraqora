using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    [SerializeField] private float life;
    public AudioSource deadEnemySound;
    private Animator animator;

    public void RecieveDamage(float damage)
    {
        life -= damage;
        if(life <= 0)
        {
            Dead();
        }
    }

    private void Dead()
    {
        Destroy(gameObject);
        deadEnemySound.Play();
    }
}
