using UnityEngine;

public class WormMovement : MonoBehaviour
{
    public Transform[] waypoints; // Array de waypoints que el gusano seguir�
    public float movementSpeed = 3f; // Velocidad de movimiento del gusano

    private int currentWaypointIndex = 0; // �ndice del waypoint actual
    private Animator animator; // Referencia al componente Animator

    private void Start()
    {
        animator = GetComponent<Animator>(); // Obtener el componente Animator
    }

    private void Update()
    {
        // Obtener la posici�n del waypoint actual
        Vector3 targetPosition = waypoints[currentWaypointIndex].position;

        // Calcular la direcci�n hacia el waypoint
        Vector3 moveDirection = (targetPosition - transform.position).normalized;

        // Mover el enemigo hacia el waypoint
        transform.position += moveDirection * movementSpeed * Time.deltaTime;

        // Verificar si se alcanz� el waypoint actual
        if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
        {
            // Avanzar al siguiente waypoint
            currentWaypointIndex++;

            // Verificar si se alcanz� el �ltimo waypoint
            if (currentWaypointIndex >= waypoints.Length)
            {
                currentWaypointIndex = 0; // Reiniciar al primer waypoint si se alcanz� el �ltimo
            }
        }

        // Calcular el movimiento horizontal y vertical
        float moveHorizontal = moveDirection.x;
        float moveVertical = moveDirection.y;

        // Actualizar la animaci�n seg�n el movimiento horizontal y vertical
        UpdateAnimation(moveHorizontal, moveVertical);
    }

    private void UpdateAnimation(float moveHorizontal, float moveVertical)
    {
        // Configurar los par�metros del Blend Tree en el Animator
        animator.SetFloat("MoveHorizontal", moveHorizontal);
        animator.SetFloat("MoveVertical", moveVertical);
    }
}