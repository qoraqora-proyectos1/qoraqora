using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject pausaPanel;
    public GameObject objectPause;
    public GameObject backToMenu;


    private bool isPaused = false;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Pausar();
            }
            else
            {
                Reanudar();
            }
        }
    }
    private void Pausar()
    {
        isPaused = true;

        pausaPanel.SetActive(true);
        objectPause.SetActive(true);

        Time.timeScale = 0.0f;

    }

    private void Reanudar()
    {
        Time.timeScale = 1.0f;
        isPaused = false;
        pausaPanel.SetActive(false);
        objectPause.SetActive(false);
    }
}