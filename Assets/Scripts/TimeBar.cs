using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeBar : MonoBehaviour
{
    public Image timeBarImage;
    public float maxTime = 180f;
    public float currentTime;

    public GameObject gameOverScreen;

    public GameObject bars;
    public GameObject inventory;

    public AudioSource derrumbe;

    public GameObject gameOver;

    public bool end = false;

    public GameObject botonmenu;

    private void Start()
    {
        currentTime = maxTime;
        UpdateBar();


    }

    private void Update()
    {
        // Reducir el tiempo actual gradualmente
        currentTime -= Time.deltaTime;
        currentTime = Mathf.Clamp(currentTime, 0f, maxTime); // Asegurarse de que el tiempo no sea menor que 0 ni mayor que el tiempo m�ximo

        UpdateBar();

        if (currentTime <= 0)
        {
            EndGame();

        }
    }

    private void UpdateBar()
    {
        // Calcular el progreso actual de la barra de tiempo
        float progress = currentTime / maxTime;

        // Actualizar el relleno de la imagen de la barra de tiempo en funci�n del progreso
        timeBarImage.fillAmount = progress;
    }

  
    private void EndGame()
    {

        end = true;
        Debug.Log("Fin del juego.");
        derrumbe.Play();

        HideThings();


    }

    private void HideThings()
    {
        gameOverScreen.SetActive(true);
        bars.SetActive(false);
        botonmenu.SetActive(false);
        inventory.SetActive(false);
        gameOver.SetActive(true);

    }

    public void IncreaseTime(float amount)
    {
        currentTime += amount;
        currentTime = Mathf.Clamp(currentTime, 0f, maxTime); // Asegurarse de que el tiempo no sea menor que 0 ni mayor que el tiempo m�ximo
        UpdateBar();
    }
}