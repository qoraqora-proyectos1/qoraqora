using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int maxHealth = 20; // Vida m�xima del enemigo
    public int currentHealth; // Vida actual del enemigo
    public float damageAmount = 10f; // Cantidad de da�o que el enemigo recibe al ser impactado por el bullet
    public bool isAlive = true;
    private bool explotar = false; // Nuevo bool para controlar si la animaci�n de explosi�n ya se reprodujo

    public Animator explosion;

    private void Start()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damageAmount)
    {
        currentHealth -= damageAmount;

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if (!explotar) // Verifica si la animaci�n ya se ha reproducido
        {
            isAlive = false;
            explosion.SetBool("explotar", true);
            explotar = true; // Actualiza el bool para indicar que la animaci�n ya se ha reproducido
            // Agrega aqu� cualquier otra l�gica que desees ejecutar despu�s de reproducir la animaci�n
            Invoke("DestroyObject", 0.5f); // Llama al m�todo DestroyObject() despu�s de 1.5 segundos (ajusta el tiempo seg�n sea necesario)
        }
    }

    private void DestroyObject()
    {
        Destroy(gameObject); // Destruye el objeto despu�s de un tiempo de espera
    }
}