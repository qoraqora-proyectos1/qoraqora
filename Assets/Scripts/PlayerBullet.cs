using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    public int damageAmount = 10; // Cantidad de da�o que causa el proyectil
    public bool chargedBullet = false;
    public bool normalBullet = true;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (chargedBullet == true)
        {
            if (collision.CompareTag("Enemy"))
            {
                return;
            }

            if (collision.CompareTag("Boss"))
            {
                return;
            }

            TorchIndividual torchIndex = collision.GetComponent<TorchIndividual>();
            if (torchIndex != null)
            {
                torchIndex.ActivateTorch();
            }
        }


        else
        {
            normalBullet = true;

            if (collision.CompareTag("Enemy"))
            {
                EnemyHealth enemyHealth = collision.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
                    enemyHealth.TakeDamage(damageAmount);
                }


            }

            if (collision.CompareTag("Boss"))
            {
                BossHealth bossHealth = collision.GetComponent<BossHealth>();
                if (bossHealth != null)
                {
                    bossHealth.TakeDamage(damageAmount);
                }


            }
        }

        Destroy(gameObject);



            Debug.Log("Destye " + collision.gameObject.name);

        }
    }

