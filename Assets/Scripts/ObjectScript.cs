using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectScript : MonoBehaviour
{
    public float scalingFactor = 0.5f;
    public Vector3 initialLocalScale;
    public PlayerController playerMovement;

    private Rigidbody2D playerRigidbody;
    public bool isSmall = false;

    public AudioSource scalePlayerSound;

    public GameObject inventoryCake;
    public AudioSource cakeCollectedSound;
    private bool cakeCollected = false; // Indicador de si hemos recogido la tarta

    public GameObject inventoryRing;
    public AudioSource ringCollectedSound;
    private bool ringCollected = false; // Indicador de si hemos recogido el anillo

    public GameObject inventoryKey;
    public AudioSource keyCollectedSound;
    public bool keyAvailable = false;

    public GameObject inventorySpinach;
    public AudioSource spinachCollectedSound;
    public float decorationSpeed = 0.5f;
    public bool spinachCollected = false; // Indicador de si hemos recogido el bote de espinacas
    public float movimientoHorizontal;
    public float movimientoVertical;

    public GlassesObject glassesObject;

    public GameObject inventoryGlasses;
    public AudioSource glassesCollectedSound;
    public bool glassesCollected = false; // Indicador de si hemos recogido las gafas

    private bool cakeActive = false;
    private bool spinachActive = false;
    public bool glassesActive = false;
    public bool ringActive = false;
    private bool keyActive = false;

    public AudioSource allObjectsSound;

    //public PlayerBullet playerBullet;


    [SerializeField] private float playerSize = 0.5f;

    public Image ringSpriteRenderer;
    public Sprite activeRing;
    public Sprite incativeRing;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        initialLocalScale = transform.localScale;
        playerMovement = GetComponent<PlayerController>();
    }

    private void Update()
    {
        movimientoHorizontal = Input.GetAxis("Horizontal");
        movimientoVertical = Input.GetAxis("Vertical");

        // Mirar si la tarta ha sido recolectada y se ha pulsado el n�mero 1
        if (cakeCollected && Input.GetKeyDown(KeyCode.Alpha1))
        {
            UseCake();
        }

        if (cakeActive == true && Input.GetKeyDown(KeyCode.K))
        {
            if (isSmall == true)
            {
                ResetPlayerSize();
            }
            else if (isSmall == false)
            {
                ScalePlayer();
            }
        }


        // Mirar si el anillo ha sido recolectada y se ha pulsado el n�mero 2
        if (ringCollected && Input.GetKeyDown(KeyCode.Alpha2))
        {
            UseRing();
        }

        // Mirar si las gafas han sido recolectadas y se ha pulsado el n�mero 3
        if (glassesCollected && Input.GetKeyDown(KeyCode.Alpha3))
        {
            UseGlasses();
        }

        // Mirar si el bote de espinacas ha sido recolectado y se ha pulsado el n�mero 4
        if (spinachCollected && Input.GetKeyDown(KeyCode.Alpha4))
        {
            UseSpinach();
        }


        // Si los 5 objetos han sido recogidos
        if (cakeCollected == true && ringCollected == true && glassesCollected == true && spinachCollected == true)
        {
            Debug.Log("AllObjects");
            allObjectsSound.Play();

        }
    }



    private void UseSpinach()
    {
        spinachActive = true;
        cakeActive = false;
        ringActive = false;
        glassesActive = false;
        //playerBullet.chargedBullet = false;
        playerMovement.useChargedBullet = false;

        Debug.Log("Espinacas Activas");
    }

    private void UseGlasses()
    {
        glassesActive = true;
        cakeActive = false;
        ringActive = false;
        spinachActive = false;
        //playerBullet.chargedBullet = false;
        playerMovement.useChargedBullet = false;
        Debug.Log("Gafas Activas");
    }

    private void UseRing()
    {
        ringActive = true;
        //playerBullet.chargedBullet = true;
        playerMovement.useChargedBullet = true;
        ringSpriteRenderer.sprite = activeRing;

        cakeActive = false;
        glassesActive = false;
        spinachActive = false;
        Debug.Log("Anillo Activo");

    }


    private void UseCake()
    {
        cakeActive = true;
        ringActive = false;
    
        //playerBullet.chargedBullet = false;
        playerMovement.useChargedBullet = false;
        glassesActive = false;
        spinachActive = false;
        Debug.Log("Tarta Activa");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Cake"))
        {
            CollectCake();
        }

        if (other.gameObject.CompareTag("Ring"))
        {
            CollectRing();
        }

        if (other.gameObject.CompareTag("Key"))
        {
            CollectKey();
        }

        if (other.gameObject.CompareTag("Spinach"))
        {
            CollectSpinach();
        }

        if (other.gameObject.CompareTag("Glasses"))
        {
            CollectGlasses();
        }


    }

    private void OnTriggerStay2D(Collider2D other)
    {
        // Comprobar si el jugador choca con un objeto de decoraci�n
        if (other.gameObject.CompareTag("Decoracion") && spinachActive == true)
        {
            float horizontalMovement = 0;
            if (playerMovement.movimientoHorizontal > 0.01f)
            {
                horizontalMovement = 1f;
            }
            else if (playerMovement.movimientoHorizontal < -0.01f)
            {
                horizontalMovement = -1f;
            }

            float verticalMovement = 0;
            if (playerMovement.movimientoVertical > 0.01f)
            {
                verticalMovement = 1f;
            }
            else if (playerMovement.movimientoVertical < -0.01f)
            {
                verticalMovement = -1f;
            }


            //float verticalMovement = Mathf.Abs(playerMovement.movimientoVertical) > 0.01f ? 1f : 0f;
            other.gameObject.transform.position = transform.position + new Vector3(playerSize * horizontalMovement, playerSize * verticalMovement, 0);
            //Vector2 direction = other.gameObject.transform.position - transform.position;
            //other.gameObject.transform.Translate(direction);
        }
    }


    private void ScalePlayer()
    {
        //playerRigidbody.transform.localScale *= scalingFactor; 
        playerRigidbody.transform.localScale = initialLocalScale * scalingFactor;
        scalePlayerSound.Play();

        isSmall = true;
        playerMovement.canShoot = false;

    }

    private void ResetPlayerSize()
    {
        //playerRigidbody.transform.localScale /= scalingFactor; 
        playerRigidbody.transform.localScale = initialLocalScale;
        isSmall = false;

        playerMovement.canShoot = true;
    }

    public void CollectCake()
    {
        cakeCollected = true; // Marcar la tarta como recogida
        Destroy(GameObject.FindGameObjectWithTag("Cake")); 

        inventoryCake.SetActive(true);
        cakeCollectedSound.Play();
 
    }

    public void CollectRing()
    {
        ringCollected = true; // Marcar el anillo como recogida
        Destroy(GameObject.FindGameObjectWithTag("Ring")); 

        inventoryRing.SetActive(true);
        ringCollectedSound.Play();
        
    }

    public void CollectKey()
    {
        keyAvailable = true;
        Destroy(GameObject.FindGameObjectWithTag("Key")); 

        inventoryKey.SetActive(true);
        keyCollectedSound.Play();

    }

    public void CollectSpinach()
    {
        spinachCollected = true; // Marcar el bote de espinacas como recogida
        Destroy(GameObject.FindGameObjectWithTag("Spinach"));

        inventorySpinach.SetActive(true);
        spinachCollectedSound.Play();

    }

    public void CollectGlasses()
    {
        glassesCollected = true; // Marcar las gafas como recogidas
        Destroy(GameObject.FindGameObjectWithTag("Glasses")); 

        inventoryGlasses.SetActive(true);
        glassesCollectedSound.Play();

    }

}