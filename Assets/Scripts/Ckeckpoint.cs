using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ckeckpoint : MonoBehaviour
{
    private PlayerHealth playerHealth; // Referencia al script de la salud del jugador

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerHealth = collision.GetComponent<PlayerHealth>();

            if (playerHealth != null)
            {
                playerHealth.SetLastCheckpointPosition(transform.position);
            }
        }
    }
}