using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossColliderDoor : MonoBehaviour
{
    public BossFinal bossFinal;

    public AudioSource musicboss;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Player"))
        {
            bossFinal.EnableBoss();

            musicboss.Play();
        }
    }
}
