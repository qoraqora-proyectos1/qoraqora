using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToPosition : MonoBehaviour
{
    public float targetX;
    public float targetY;

    public AudioSource transport;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            transport.Play();
            other.transform.position = new Vector2(targetX, targetY);
        }
    }
}