using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float velocidad = 2f;
    public bool isActive = true;

    public bool isMoving = false;
    public bool isMovingHorizontal = false;

    private bool isMovinghorizontal = false;
    private bool isMovingVertical = false;

    public float movimientoHorizontal;
    public float movimientoVertical;

    private Rigidbody2D playerRigidbody;

    public GameObject bulletPrefab;
    public GameObject chargedBulletPrefab;

    public bool useChargedBullet = false;

    public Transform firePoint;
    public float bulletForce = 10f;

    public bool canShoot = true;
    public bool canMove = true;

    public ObjectScript objectScript;
    public PlayerBullet playerBullet;

    private Animator animator;

    public TimeBar timeBar;
    public AudioSource derrumbe;

    public AudioSource disparo;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        movimientoHorizontal = Input.GetAxis("Horizontal");
        movimientoVertical = Input.GetAxis("Vertical");

        Vector2 movimiento = new Vector2(movimientoHorizontal, movimientoVertical).normalized;

        playerRigidbody.velocity = movimiento * velocidad;

        // Actualizar los parámetros "Horizontal" y "Vertical" en el Animator
        if (movimientoHorizontal != 0f)
        {
            isMovinghorizontal = true;
            isMovingVertical = false;
            animator.SetFloat("Horizontal", movimientoHorizontal);
            animator.SetFloat("Vertical", 0f);
        }
        else if (movimientoVertical != 0f)
        {
            isMovinghorizontal = false;
            isMovingVertical = true;
            animator.SetFloat("Horizontal", 0f);
            animator.SetFloat("Vertical", movimientoVertical);
        }
        else
        {
            isMovinghorizontal = false;
            isMovingVertical = false;
        }

        // Actualizar el parámetro "Speed" en el Animator
        float speed = movimiento.magnitude > 0f ? 1f : 0f;
        animator.SetFloat("Speed", speed);

        if (Input.GetKeyDown(KeyCode.K) && canShoot)
        {
            Shoot();
        }

        if (timeBar.end == true)
        {
            derrumbe.Play();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Detener el movimiento del personaje al colisionar con otro objeto o pared
        playerRigidbody.velocity = Vector2.zero;
    }

    private void Shoot()
    {
        disparo.Play();

        if (canShoot)
        {
            Debug.Log("Disparo");
            GameObject bulletToInstantiate = bulletPrefab;
            if (useChargedBullet)
            {
                bulletToInstantiate = chargedBulletPrefab;

                useChargedBullet = false;

                objectScript.ringSpriteRenderer.sprite = objectScript.incativeRing;
            }

            

                Vector2 shootDirection;
            if (movimientoHorizontal != 0f || movimientoVertical != 0f)
            {
                shootDirection = new Vector2(movimientoHorizontal, movimientoVertical).normalized;
            }

            else
            {
                shootDirection = firePoint.right;
            }

            GameObject bullet = Instantiate(bulletToInstantiate, firePoint.position, Quaternion.identity);

            Rigidbody2D bulletRB = bullet.GetComponent<Rigidbody2D>();
            bulletRB.AddForce(shootDirection * bulletForce, ForceMode2D.Impulse);
        }
    }


}