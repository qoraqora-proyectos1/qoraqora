using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFinal : MonoBehaviour
{
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;
    public float movementSpeed = 3f;
    public GameObject miniEnemyPrefab;
    public Transform miniEnemySpawnPoint;
    public float miniEnemySpawnInterval = 10f;

    private bool activated = false;
    private float nextMiniEnemySpawnTime;
    private Vector3 targetPosition;

    private void Start()
    {
        DisableBoss();
    }

    private void Update()
    {
        if (activated)
        {
            // L�gica de movimiento aleatorio del jefe
            if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
            {
                GenerateRandomTargetPosition();
            }
            MoveTowardsTargetPosition();

            // L�gica de generaci�n de mini enemigos
            if (Time.time >= nextMiniEnemySpawnTime)
            {
                SpawnMiniEnemy();
                nextMiniEnemySpawnTime = Time.time + miniEnemySpawnInterval;
            }
        }
    }

    public void EnableBoss()
    {
        activated = true;
        GenerateRandomTargetPosition();
    }

    private void DisableBoss()
    {
        activated = false;
    }

    private void GenerateRandomTargetPosition()
    {
        float randomX = Random.Range(minX, maxX);
        float randomY = Random.Range(minY, maxY);
        targetPosition = new Vector3(randomX, randomY, 0f);
    }

    private void MoveTowardsTargetPosition()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, movementSpeed * Time.deltaTime);
    }

    private void SpawnMiniEnemy()
    {
        Instantiate(miniEnemyPrefab, miniEnemySpawnPoint.position, Quaternion.identity);
    }
}