using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public GameObject[] optionsOfStore;

    public void Start()
    {
        int randomValue = Random.Range(0, optionsOfStore.Length);
        for (int i = 0; i < optionsOfStore.Length; i++)
        {
            if (i != randomValue)
            {
                optionsOfStore[i].SetActive(true);
            }
        }
    }
}