using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public float maxHealth = 100f; // Vida m�xima del jugador
    public float currentHealth; // Vida actual del jugador
    public Vector3 lastCheckpointPosition; // Posici�n del �ltimo punto de control alcanzado

    public HealthBar healthBar;

    public GameObject textCheckpoint;

    public SpriteRenderer spriteRenderer;

    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.UpdateBar();
    }

    public void TakeDamage(float damageAmount)
    {
        currentHealth -= damageAmount;
        currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);

        healthBar.UpdateBar();

        if (currentHealth <= 0f)
        {
            Die();
        }
    }

    public void IncreaseHealth(float amount)
    {
        currentHealth += amount;
        currentHealth = Mathf.Clamp(currentHealth, 0f, maxHealth);

        healthBar.UpdateBar();
    }

    private void Die()
    {
        transform.position = lastCheckpointPosition;
        currentHealth = maxHealth;
        healthBar.UpdateBar();
        StartCoroutine(BlinkSprite(3, 0.3f));
    }

    private System.Collections.IEnumerator BlinkSprite(int blinkCount, float blinkDuration)
    {
        for (int i = 0; i < blinkCount; i++)
        {
            spriteRenderer.enabled = false;
            yield return new WaitForSeconds(blinkDuration);
            spriteRenderer.enabled = true;
            yield return new WaitForSeconds(blinkDuration);
        }
    }

    public void SetLastCheckpointPosition(Vector3 position)
    {
        lastCheckpointPosition = position;

        StartCoroutine(ShowAndHide());
    }

    private IEnumerator ShowAndHide()
    {
        // Mostrar la indicaci�n del punto de control
        // (asume que tienes una variable "checkpointIndicator" que representa el objeto a mostrar)
        textCheckpoint.SetActive(true);

        // Esperar 3 segundos
        yield return new WaitForSeconds(3f);

        // Ocultar la indicaci�n del punto de control
        textCheckpoint.SetActive(false);
    }

}