using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlassesObject : MonoBehaviour
{
    public GameObject hiddenObjects;
    public ObjectScript objectScript;
    private bool glassesActivated = false;

    public GameObject gridNormal;


    // Update is called once per frame
    void Update()
    {
        if (objectScript.glassesActive && Input.GetKeyDown(KeyCode.J))
        {
            if (glassesActivated)
            {
                DeactivateGlasses();
                Debug.Log("Se han desactivado las gafas");
            }
            else
            {
                ActivateGlasses();
                Debug.Log("Se han activado las gafas");
            }

        }

    }
    public void ActivateGlasses()
        {
            hiddenObjects.SetActive(true);
            glassesActivated = true;
            gridNormal.SetActive(false);

        }

        public void DeactivateGlasses()
        {
            hiddenObjects.SetActive(false);
            glassesActivated = false;
            gridNormal.SetActive(true);
    }
}