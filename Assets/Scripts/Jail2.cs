using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jail2 : MonoBehaviour
{
    public Animator jailAnimator;
    public ObjectScript objectScript;
    public EnemyHealth fly1;
    public EnemyHealth fly2;
    public EnemyHealth fly3;
    public AudioSource openJail;

    private void Update()
    {
        if (fly1.isAlive == false && fly2.isAlive == false && fly3.isAlive == false)
        {
            OpenDoor();
            openJail.Play();

        }
    }

    private void OpenDoor()
    {
        jailAnimator.SetBool("Open", true);
        StartCoroutine(DestroyDoorAfterDelay(2f));
    }

    private IEnumerator DestroyDoorAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(gameObject);
    }
}