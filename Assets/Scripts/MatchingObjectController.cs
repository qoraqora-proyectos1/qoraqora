using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchingObjectController : MonoBehaviour
{
    //private static MatchingObjectController instance;
    //public static MatchingObjectController Instance => instance;

    public GameObject doorObject;

    //private void Awake()
    //{
    //    if (instance == null)
    //    {
    //        instance = this;
    //    }
    //    else
    //    {
    //        Destroy(this);
    //    }
    //}

    public Animator doorAnimator;
    public AudioSource openDoor;

    public MatchingObjects[] matchingObjects;
    public GameObject coloredObjectsParent; 

    //Esto hace que se puede ejectuar un metodo desde el inspector haciendo clic derecho.
    [ContextMenu("Open Door")]
    private void OpenDoor()
    {
        Debug.Log("Open Door");
        doorAnimator.SetBool("Open", true);
        StartCoroutine(DestroyDoorAfterDelay(2f));
    }

    private IEnumerator DestroyDoorAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(doorObject);
    }

    public void CheckAllObjectsActivated()
    {
        bool solved = true;
        for (int i = 0; i < matchingObjects.Length; i++)
        {
            if(!matchingObjects[i].matchingJarInPlace)
            { 
                solved = false;
            }
        }
        if(solved)
        {
            OpenDoor();
        }
    }    
}