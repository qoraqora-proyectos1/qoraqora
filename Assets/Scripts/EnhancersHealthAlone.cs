using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnhancersHealthAlone : MonoBehaviour
{
    public float healthIncreaseAmount = 20f; // Cantidad de aumento de salud al recoger el objeto


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            if (playerHealth != null)
            {
                playerHealth.IncreaseHealth(healthIncreaseAmount);
                Debug.Log("Aumenta la vida por el potenciador");

                Destroy(gameObject); // Destruir el objeto al ser recogido
            }
        }
    }
}
